#!/bin/bash -e

#Install make & gcc for Radare2
sudo apt install -y gcc make
# Install Radare2
git clone https://github.com/radareorg/radare2
radare2/sys/install.sh

# Install 32bit support
sudo dpkg --add-architecture i386
sudo apt-get update
sudo apt-get install -y libc6:i386 libncurses5:i386 libstdc++6:i386

# Install unzip
sudo apt install -y unzip

# Install micro editor
sudo apt install -y micro

# Install python
sudo apt install -y python3-pip
sudo apt install -y python3-dev python3-setuptools

# Install GDB and Pwndbg
sudo apt install -y gdb
git clone https://github.com/pwndbg/pwndbg
cd pwndbg && ./setup.sh && cd ~